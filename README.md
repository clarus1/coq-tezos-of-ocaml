# ꜩ 🐓 coq-tezos-of-ocaml
> Formalization of the source code of [Tezos](https://tezos.com/) in [Coq](https://coq.inria.fr/)

We provide a formalization of various parts and versions of the [Tezos codebase](https://gitlab.com/tezos/tezos) using [coq-of-ocaml](https://github.com/clarus/coq-of-ocaml). This could be used to do formal verification of the Tezos blockchain. We formalize the OCaml implementation of Tezos using a mostly automatic method with coq-of-ocaml. We generate a shallow embedding in Coq, hoping to have a formalization which is easy to read and manipulate.

## Install
Using opam, run from the command line:
```
opam install .
```

To do a manual build, look at the instructions in the opam file.

## Content
* `Proto_005_PsBabyM1` the first protocol version for which we generate a formalization for all the files. Look at [coq-of-ocaml examples](https://clarus.github.io/coq-of-ocaml/examples/tezos/) for more information;
* `Proto_alpha` the current development version of Tezos. To generate the Coq code, run the `import.rb` script from `scripts/alpha`.
