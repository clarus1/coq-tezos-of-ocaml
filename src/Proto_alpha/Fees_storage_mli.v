(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Import Environment.Notations.

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

Parameter origination_burn :
  Raw_context.t -> Lwt.t (Error_monad.tzresult (Raw_context.t * Tez_repr.t)).

Parameter record_paid_storage_space :
  Raw_context.t -> Contract_repr.t ->
  Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t * Z.t * Tez_repr.t)).

Parameter check_storage_limit :
  Raw_context.t -> Z.t -> Error_monad.tzresult unit.

Parameter start_counting_storage_fees : Raw_context.t -> Raw_context.t.

Parameter burn_storage_fees :
  Raw_context.t -> Z.t -> Contract_repr.t ->
  Lwt.t (Error_monad.tzresult Raw_context.t).
