(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Contract_repr.

Import Environment.Notations.

Import Alpha_context.

Parameter __list_value : forall {a : Set},
  RPC_context.simple a -> a -> option bool -> option bool -> unit ->
  Lwt.t
    (Error_monad.shell_tzresult
      (list (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t))).

Module info.
  Record record : Set := Build {
    balance : Alpha_context.Tez.t;
    frozen_balance : Alpha_context.Tez.t;
    frozen_balance_by_cycle :
      (|Alpha_context.Cycle.Map|).(S.MAP.t)
        Alpha_context.Delegate.frozen_balance;
    staking_balance : Alpha_context.Tez.t;
    delegated_contracts : list Contract_repr.t;
    delegated_balance : Alpha_context.Tez.t;
    deactivated : bool;
    grace_period : Alpha_context.Cycle.t;
    voting_power : int32;
    proof_levels : Alpha_context.Raw_level.LSet.t }.
  Definition with_balance balance (r : record) :=
    Build balance r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_frozen_balance frozen_balance (r : record) :=
    Build r.(balance) frozen_balance r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_frozen_balance_by_cycle frozen_balance_by_cycle
    (r : record) :=
    Build r.(balance) r.(frozen_balance) frozen_balance_by_cycle
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_staking_balance staking_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      staking_balance r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_delegated_contracts delegated_contracts (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) delegated_contracts r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_delegated_balance delegated_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) delegated_balance
      r.(deactivated) r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_deactivated deactivated (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      deactivated r.(grace_period) r.(voting_power) r.(proof_levels).
  Definition with_grace_period grace_period (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) grace_period r.(voting_power) r.(proof_levels).
  Definition with_voting_power voting_power (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) voting_power r.(proof_levels).
  Definition with_proof_levels proof_levels (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power) proof_levels.
End info.
Definition info := info.record.

Parameter info_encoding : Data_encoding.t info.

Parameter __info_value : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult info).

Parameter balance : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Tez.t).

Parameter __frozen_balance_value : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Tez.t).

Parameter frozen_balance_by_cycle : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t
    (Error_monad.shell_tzresult
      ((|Alpha_context.Cycle.Map|).(S.MAP.t)
        Alpha_context.Delegate.frozen_balance)).

Parameter staking_balance : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Tez.t).

Parameter delegated_contracts : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult (list Contract_repr.t)).

Parameter delegated_balance : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Tez.t).

Parameter deactivated : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult bool).

Parameter grace_period : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Cycle.t).

Parameter voting_power : forall {a : Set},
  RPC_context.simple a -> a -> Alpha_context.public_key_hash ->
  Lwt.t (Error_monad.shell_tzresult int32).

Parameter proof_levels : forall {a : Set},
  RPC_context.simple a -> a ->
  (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t) ->
  Lwt.t (Error_monad.shell_tzresult Alpha_context.Raw_level.LSet.t).

Module Baking_rights.
  Module t.
    Record record : Set := Build {
      level : Alpha_context.Raw_level.t;
      delegate : (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t);
      priority : int;
      timestamp : option Alpha_context.Timestamp.t }.
    Definition with_level level (r : record) :=
      Build level r.(delegate) r.(priority) r.(timestamp).
    Definition with_delegate delegate (r : record) :=
      Build r.(level) delegate r.(priority) r.(timestamp).
    Definition with_priority priority (r : record) :=
      Build r.(level) r.(delegate) priority r.(timestamp).
    Definition with_timestamp timestamp (r : record) :=
      Build r.(level) r.(delegate) r.(priority) timestamp.
  End t.
  Definition t := t.record.
  
  Parameter get : forall {a : Set},
    RPC_context.simple a -> option (list Alpha_context.Raw_level.t) ->
    option (list Alpha_context.Cycle.t) ->
    option (list Signature.public_key_hash) -> option bool -> option int -> a ->
    Lwt.t (Error_monad.shell_tzresult (list t)).
End Baking_rights.

Module Endorsing_rights.
  Module t.
    Record record : Set := Build {
      level : Alpha_context.Raw_level.t;
      delegate : (|Signature.Public_key_hash|).(S.SIGNATURE_Public_key_hash.t);
      slots : list int;
      estimated_time : option Alpha_context.Timestamp.t }.
    Definition with_level level (r : record) :=
      Build level r.(delegate) r.(slots) r.(estimated_time).
    Definition with_delegate delegate (r : record) :=
      Build r.(level) delegate r.(slots) r.(estimated_time).
    Definition with_slots slots (r : record) :=
      Build r.(level) r.(delegate) slots r.(estimated_time).
    Definition with_estimated_time estimated_time (r : record) :=
      Build r.(level) r.(delegate) r.(slots) estimated_time.
  End t.
  Definition t := t.record.
  
  Parameter get : forall {a : Set},
    RPC_context.simple a -> option (list Alpha_context.Raw_level.t) ->
    option (list Alpha_context.Cycle.t) ->
    option (list Signature.public_key_hash) -> a ->
    Lwt.t (Error_monad.shell_tzresult (list t)).
End Endorsing_rights.

Module Endorsing_power.
  Parameter get : forall {a : Set},
    RPC_context.simple a -> a -> Alpha_context.packed_operation ->
    (|Chain_id|).(S.HASH.t) -> Lwt.t (Error_monad.shell_tzresult int).
End Endorsing_power.

Module Required_endorsements.
  Parameter get : forall {a : Set},
    RPC_context.simple a -> a -> Alpha_context.Period.t ->
    Lwt.t (Error_monad.shell_tzresult int).
End Required_endorsements.

Module Minimal_valid_time.
  Parameter get : forall {a : Set},
    RPC_context.simple a -> a -> int -> int ->
    Lwt.t (Error_monad.shell_tzresult Time.t).
End Minimal_valid_time.

Parameter endorsement_rights :
  Alpha_context.t -> Alpha_context.Level.t ->
  Lwt.t (Error_monad.tzresult (list Alpha_context.public_key_hash)).

Parameter baking_rights :
  Alpha_context.t -> option int ->
  Lwt.t
    (Error_monad.tzresult
      (Alpha_context.Raw_level.t *
        list (Alpha_context.public_key_hash * option Time.t))).

Parameter endorsing_power :
  Alpha_context.t -> Alpha_context.packed_operation * (|Chain_id|).(S.HASH.t) ->
  Lwt.t (Error_monad.tzresult int).

Parameter required_endorsements :
  Alpha_context.t -> Alpha_context.Period.t -> Lwt.t (Error_monad.tzresult int).

Parameter minimal_valid_time :
  Alpha_context.t -> int -> int -> Lwt.t (Error_monad.tzresult Time.t).

Parameter register : unit -> unit.
