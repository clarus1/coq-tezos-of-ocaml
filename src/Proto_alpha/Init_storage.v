(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Bootstrap_storage.
Require TezosOfOCaml.Proto_alpha.Commitment_storage.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Contract_storage.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Parameters_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Roll_storage.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Seed_storage.
Require TezosOfOCaml.Proto_alpha.Storage_mli. Module Storage := Storage_mli.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Vote_storage.

Import Environment.Notations.

Definition invoice_contract
  (ctxt : Raw_context.t) (address : string) (amount : Tez_repr.t)
  : Lwt.t (Error_monad.tzresult (Raw_context.t * Receipt_repr.balance_updates)) :=
  match Contract_repr.of_b58check address with
  | Pervasives.Ok recipient =>
    let=? function_parameter := Contract_storage.allocated ctxt recipient in
    match function_parameter with
    | false => Error_monad.__return (ctxt, nil)
    | true =>
      let=? ctxt := Contract_storage.credit ctxt recipient amount in
      Error_monad.__return
        (ctxt,
          [ ((Receipt_repr.Contract recipient), (Receipt_repr.Credited amount)) ])
    end
  | Pervasives.Error _ => Error_monad.__return (ctxt, nil)
  end.

Definition prepare_first_block
  (ctxt : Context.t)
  (typecheck :
    Raw_context.t -> Script_repr.t ->
    Lwt.t
      (Error_monad.tzresult
        ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t)))
  (level : int32) (timestamp : Time.t) (fitness : (|Fitness|).(S.T.t))
  : Lwt.t (Error_monad.tzresult Raw_context.t) :=
  let=? '(previous_protocol, ctxt) :=
    Raw_context.prepare_first_block level timestamp fitness ctxt in
  match previous_protocol with
  | Raw_context.Genesis param =>
    let=? ctxt :=
      Commitment_storage.init ctxt param.(Parameters_repr.t.commitments) in
    let=? ctxt := Roll_storage.init ctxt in
    let=? ctxt := Seed_storage.init ctxt in
    let=? ctxt := Contract_storage.init ctxt in
    let=? ctxt :=
      Bootstrap_storage.init ctxt typecheck
        param.(Parameters_repr.t.security_deposit_ramp_up_cycles)
        param.(Parameters_repr.t.no_reward_cycles)
        param.(Parameters_repr.t.bootstrap_accounts)
        param.(Parameters_repr.t.bootstrap_contracts) in
    let=? ctxt := Roll_storage.init_first_cycles ctxt in
    let=? ctxt := Vote_storage.init ctxt in
    let=? ctxt := Storage.Block_priority.init ctxt 0 in
    Vote_storage.update_listings ctxt
  | Raw_context.Carthage_006 =>
    let amount :=
      Tez_repr.of_mutez_exn
        (* ❌ Constant of type int64 is converted to int *)
        662607015 in
    let=? '(ctxt, balance_updates) :=
      invoice_contract ctxt "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" amount in
    (|Storage.Pending_migration_balance_updates|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init)
      ctxt balance_updates
  end.

Definition prepare
  (ctxt : Context.t) (level : Int32.t) (predecessor_timestamp : Time.t)
  (timestamp : Time.t) (fitness : (|Fitness|).(S.T.t))
  : Lwt.t
    (Error_monad.tzresult
      (Raw_context.t *
        (|Storage.Pending_migration_balance_updates|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.value))) :=
  let=? ctxt :=
    Raw_context.prepare level predecessor_timestamp timestamp fitness ctxt in
  let=? function_parameter :=
    (|Storage.Pending_migration_balance_updates|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option)
      ctxt in
  match function_parameter with
  | Some balance_updates =>
    let= ctxt :=
      (|Storage.Pending_migration_balance_updates|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
        ctxt in
    Error_monad.__return (ctxt, balance_updates)
  | None => Error_monad.__return (ctxt, nil)
  end.
