(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Storage_mli. Module Storage := Storage_mli.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.

Import Environment.Notations.

Module Next.
  Record signature {id : Set} : Set := {
    id := id;
    init : Raw_context.t -> Lwt.t (Error_monad.tzresult Raw_context.t);
    incr : Raw_context.t -> Lwt.t (Error_monad.tzresult (Raw_context.t * id));
  }.
End Next.

Module Total_bytes.
  Record signature {id : Set} : Set := {
    id := id;
    init :
      Raw_context.t -> id -> Z.t -> Lwt.t (Error_monad.tzresult Raw_context.t);
    get : Raw_context.t -> id -> Lwt.t (Error_monad.tzresult Z.t);
    set :
      Raw_context.t -> id -> Z.t -> Lwt.t (Error_monad.tzresult Raw_context.t);
  }.
End Total_bytes.

Module OPS.
  Record signature {Id_t alloc updates : Set} : Set := {
    Id : Lazy_storage_kind.ID.signature (t := Id_t);
    alloc := alloc;
    updates := updates;
    title : string;
    alloc_encoding : Data_encoding.t alloc;
    updates_encoding : Data_encoding.t updates;
    bytes_size_for_empty : Z.t;
    alloc :
      Raw_context.t -> Id.(Lazy_storage_kind.ID.t) -> alloc ->
      Lwt.t (Error_monad.tzresult Raw_context.t);
    apply_updates :
      Raw_context.t -> Id.(Lazy_storage_kind.ID.t) -> updates ->
      Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t));
    Next : Next.signature (id := Id.(Lazy_storage_kind.ID.t));
    Total_bytes : Total_bytes.signature (id := Id.(Lazy_storage_kind.ID.t));
    copy :
      Raw_context.t -> Id.(Lazy_storage_kind.ID.t) ->
      Id.(Lazy_storage_kind.ID.t) -> Lwt.t (Error_monad.tzresult Raw_context.t);
    remove_rec :
      Raw_context.t -> Id.(Lazy_storage_kind.ID.t) -> Lwt.t Raw_context.t;
  }.
End OPS.

Module Big_map.
  Include Lazy_storage_kind.Big_map.
  
  Definition bytes_size_for_big_map_key : int := 65.
  
  Definition bytes_size_for_empty : Z.t :=
    let bytes_size_for_big_map := 33 in
    Z.of_int bytes_size_for_big_map.
  
  Definition alloc
    (ctxt :
      (|Storage.Big_map.Key_type|).(Storage_sigs.Indexed_data_storage.context))
    (id : (|Storage.Big_map.Key_type|).(Storage_sigs.Indexed_data_storage.key))
    (function_parameter : alloc) : Lwt.t (Error_monad.tzresult Raw_context.t) :=
    let '{|
      Lazy_storage_kind.Big_map.alloc.key_type := key_type;
        Lazy_storage_kind.Big_map.alloc.value_type := value_type
        |} := function_parameter in
    let key_type :=
      Micheline.strip_locations
        (Script_repr.strip_annotations (Micheline.root key_type)) in
    let value_type :=
      Micheline.strip_locations
        (Script_repr.strip_annotations (Micheline.root value_type)) in
    let=? ctxt :=
      (|Storage.Big_map.Key_type|).(Storage_sigs.Indexed_data_storage.init) ctxt
        id key_type in
    (|Storage.Big_map.Value_type|).(Storage_sigs.Indexed_data_storage.init) ctxt
      id value_type.
  
  Definition apply_update
    (ctxt : Raw_context.t) (id : Storage.Big_map.id)
    (function_parameter : update)
    : Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
    let '{|
      Lazy_storage_kind.Big_map.update.key :=
        _key_is_shown_only_on_the_receipt_in_print_big_map_diff;
        Lazy_storage_kind.Big_map.update.key_hash := key_hash;
        Lazy_storage_kind.Big_map.update.value := value
        |} := function_parameter in
    match value with
    | None =>
      Error_monad.op_gtpipeeqquestion
        ((|Storage.Big_map.Contents|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
          (ctxt, id) key_hash)
        (fun function_parameter =>
          let '(ctxt, freed, existed) := function_parameter in
          let freed :=
            if existed then
              Pervasives.op_plus freed bytes_size_for_big_map_key
            else
              freed in
          (ctxt, (Z.of_int (Pervasives.op_tildeminus freed))))
    | Some v =>
      Error_monad.op_gtpipeeqquestion
        ((|Storage.Big_map.Contents|).(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set)
          (ctxt, id) key_hash v)
        (fun function_parameter =>
          let '(ctxt, size_diff, existed) := function_parameter in
          let size_diff :=
            if existed then
              size_diff
            else
              Pervasives.op_plus size_diff bytes_size_for_big_map_key in
          (ctxt, (Z.of_int size_diff)))
    end.
  
  Definition apply_updates
    (ctxt : Raw_context.t) (id : Storage.Big_map.id) (updates : list update)
    : Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
    Error_monad.fold_left_s
      (fun function_parameter =>
        let '(ctxt, size) := function_parameter in
        fun update =>
          Error_monad.op_gtpipeeqquestion (apply_update ctxt id update)
            (fun function_parameter =>
              let '(ctxt, added_size) := function_parameter in
              (ctxt, (Z.add size added_size)))) (ctxt, Z.zero) updates.
  
  Include Storage.Big_map.
End Big_map.

Definition ops (id alloc updates : Set) : Set :=
  {_ : unit @ OPS.signature (Id_t := id) (alloc := alloc) (updates := updates)}.

Definition get_ops {a i u : Set} (function_parameter : Lazy_storage_kind.t)
  : ops i a u :=
  let 'Lazy_storage_kind.Big_map := function_parameter in
  (pack
    (existT (A := unit) (fun _ => _) tt
      {|
        OPS.Id := (|Id|);
        OPS.title := Big_map.title;
        OPS.alloc_encoding := Big_map.alloc_encoding;
        OPS.updates_encoding := Big_map.updates_encoding;
        OPS.bytes_size_for_empty := Big_map.bytes_size_for_empty;
        OPS.alloc := Big_map.alloc;
        OPS.apply_updates := Big_map.apply_updates;
        OPS.Next := (|Next|);
        OPS.Total_bytes := (|Total_bytes|);
        OPS.copy := Big_map.copy;
        OPS.remove_rec := Big_map.remove_rec
      |})).

Module KId.
  Inductive t : Set :=
  | E : forall {id : Set}, Lazy_storage_kind.t -> id -> t.
  
  Definition Comparable :=
    let t : Set := t in
    let compare (function_parameter : t) : t -> int :=
      let 'E kind1 id1 := function_parameter in
      let 'existT _ __E_'id [kind1, id1] :=
        existT (A := Set) (fun __E_'id => [Lazy_storage_kind.t ** __E_'id]) _
          [kind1, id1] in
      fun function_parameter =>
        let 'E kind2 id2 := function_parameter in
        let 'existT _ __E_'id1 [kind2, id2] :=
          existT (A := Set) (fun __E_'id1 => [Lazy_storage_kind.t ** __E_'id1])
            _ [kind2, id2] in
        match Lazy_storage_kind.compare kind1 kind2 with
        | Lazy_storage_kind.Lt => (-1)
        | Lazy_storage_kind.Gt => 1
        | Lazy_storage_kind.Eq =>
          let OPS := get_ops kind1 in
          let 'existS _ _ OPS := OPS in
          OPS.(OPS.Id).(Lazy_storage_kind.ID.compare) id1 id2
        end in
    existT (A := unit) (fun _ => _) tt
      {|
        Compare.COMPARABLE.compare := compare
      |}.
End KId.

Module ConstructorRecords_init.
  Module init.
    Module Copy.
      Record record {src : Set} : Set := Build {
        src : src }.
      Arguments record : clear implicits.
      Definition with_src {t_src} src (r : record t_src) :=
        Build t_src src.
    End Copy.
    Definition Copy_skeleton := Copy.record.
  End init.
End ConstructorRecords_init.
Import ConstructorRecords_init.

Reserved Notation "'init.Copy".

Inductive init (id alloc : Set) : Set :=
| Existing : init id alloc
| Copy : 'init.Copy id -> init id alloc
| Alloc : alloc -> init id alloc

where "'init.Copy" := (fun (t_id : Set) => init.Copy_skeleton t_id).

Module init.
  Include ConstructorRecords_init.init.
  Definition Copy := 'init.Copy.
End init.

Arguments Existing {_ _}.
Arguments Copy {_ _}.
Arguments Alloc {_ _}.

Module ConstructorRecords_diff.
  Module diff.
    Module Update.
      Record record {init updates : Set} : Set := Build {
        init : init;
        updates : updates }.
      Arguments record : clear implicits.
      Definition with_init {t_init t_updates} init
        (r : record t_init t_updates) :=
        Build t_init t_updates init r.(updates).
      Definition with_updates {t_init t_updates} updates
        (r : record t_init t_updates) :=
        Build t_init t_updates r.(init) updates.
    End Update.
    Definition Update_skeleton := Update.record.
  End diff.
End ConstructorRecords_diff.
Import ConstructorRecords_diff.

Reserved Notation "'diff.Update".

Inductive diff (id alloc updates : Set) : Set :=
| Remove : diff id alloc updates
| Update : 'diff.Update alloc id updates -> diff id alloc updates

where "'diff.Update" := (fun (t_alloc t_id t_updates : Set) =>
  diff.Update_skeleton (init t_id t_alloc) t_updates).

Module diff.
  Include ConstructorRecords_diff.diff.
  Definition Update := 'diff.Update.
End diff.

Arguments Remove {_ _ _}.
Arguments Update {_ _ _}.

Definition diff_encoding {a i u : Set} (OPS : ops i a u)
  : Data_encoding.t (diff i a u) :=
  let 'existS _ _ OPS := OPS in
  Data_encoding.union None
    [
      Data_encoding.__case_value "update" None (Data_encoding.Tag 0)
        (Data_encoding.obj2
          (Data_encoding.req None None "action"
            (Data_encoding.constant "update"))
          (Data_encoding.req None None "updates" OPS.(OPS.updates_encoding)))
        (fun function_parameter =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Existing;
                diff.Update.updates := updates
                |} => Some (tt, updates)
          | _ => None
          end)
        (fun function_parameter =>
          let '(_, updates) := function_parameter in
          Update
            {| diff.Update.init := Existing;
              diff.Update.updates := updates |});
      Data_encoding.__case_value "remove" None (Data_encoding.Tag 1)
        (Data_encoding.obj1
          (Data_encoding.req None None "action"
            (Data_encoding.constant "remove")))
        (fun function_parameter =>
          match function_parameter with
          | Remove => Some tt
          | _ => None
          end)
        (fun function_parameter =>
          let '_ := function_parameter in
          Remove);
      Data_encoding.__case_value "copy" None (Data_encoding.Tag 2)
        (Data_encoding.obj3
          (Data_encoding.req None None "action"
            (Data_encoding.constant "copy"))
          (Data_encoding.req None None "source"
            OPS.(OPS.Id).(Lazy_storage_kind.ID.encoding))
          (Data_encoding.req None None "updates" OPS.(OPS.updates_encoding)))
        (fun function_parameter =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Copy {| init.Copy.src := src |};
                diff.Update.updates := updates
                |} => Some (tt, src, updates)
          | _ => None
          end)
        (fun function_parameter =>
          let '(_, src, updates) := function_parameter in
          Update
            {| diff.Update.init := Copy {| init.Copy.src := src |};
              diff.Update.updates := updates |});
      Data_encoding.__case_value "alloc" None (Data_encoding.Tag 3)
        (Data_encoding.merge_objs
          (Data_encoding.obj2
            (Data_encoding.req None None "action"
              (Data_encoding.constant "alloc"))
            (Data_encoding.req None None "updates"
              OPS.(OPS.updates_encoding))) OPS.(OPS.alloc_encoding))
        (fun function_parameter =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Alloc alloc;
                diff.Update.updates := updates
                |} => Some ((tt, updates), alloc)
          | _ => None
          end)
        (fun function_parameter =>
          let '((_, updates), alloc) := function_parameter in
          Update
            {| diff.Update.init := Alloc alloc;
              diff.Update.updates := updates |})
    ].

Definition apply_updates {a i u : Set} (ctxt : Raw_context.t) (OPS : ops i a u)
  : i -> u -> Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
  let 'existS _ _ OPS := OPS in
  fun id =>
    fun updates =>
      let=? '(ctxt, updates_size) := OPS.(OPS.apply_updates) ctxt id updates in
      if Z.equal updates_size Z.zero then
        Error_monad.__return (ctxt, updates_size)
      else
        let=? size := OPS.(OPS.Total_bytes).(Total_bytes.get) ctxt id in
        Error_monad.op_gtpipeeqquestion
          (OPS.(OPS.Total_bytes).(Total_bytes.set) ctxt id
            (Z.add size updates_size)) (fun ctxt => (ctxt, updates_size)).

Definition apply_init {a i u : Set} (ctxt : Raw_context.t) (OPS : ops i a u)
  : i -> init i a -> Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
  let 'existS _ _ OPS := OPS in
  fun id =>
    fun init =>
      match init with
      | Existing => Error_monad.__return (ctxt, Z.zero)
      | Copy {| init.Copy.src := src |} =>
        let=? ctxt := OPS.(OPS.copy) ctxt src id in
        let=? copy_size := OPS.(OPS.Total_bytes).(Total_bytes.get) ctxt src in
        Error_monad.__return
          (ctxt, (Z.add copy_size OPS.(OPS.bytes_size_for_empty)))
      | Alloc alloc =>
        let=? ctxt := OPS.(OPS.Total_bytes).(Total_bytes.init) ctxt id Z.zero in
        let=? ctxt := OPS.(OPS.alloc) ctxt id alloc in
        Error_monad.__return (ctxt, OPS.(OPS.bytes_size_for_empty))
      end.

Definition apply_diff {a i u : Set}
  (ctxt : Raw_context.t) (function_parameter : ops i a u)
  : i -> diff i a u -> Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
  let 'OPS as ops := function_parameter in
  let 'existS _ _ OPS := OPS in
  fun id =>
    fun diff =>
      match diff with
      | Remove =>
        let=? size := OPS.(OPS.Total_bytes).(Total_bytes.get) ctxt id in
        let= ctxt := OPS.(OPS.remove_rec) ctxt id in
        Error_monad.__return
          (ctxt, (Z.neg (Z.add size OPS.(OPS.bytes_size_for_empty))))
      | Update {| diff.Update.init := init; diff.Update.updates := updates |} =>
        let=? '(ctxt, init_size) := apply_init ctxt ops id init in
        let=? '(ctxt, updates_size) := apply_updates ctxt ops id updates in
        Error_monad.__return (ctxt, (Z.add init_size updates_size))
      end.

Inductive diffs_item : Set :=
| E : forall {a i u : Set}, Lazy_storage_kind.t -> i -> diff i a u -> diffs_item.

Definition make {a i u : Set}
  (k : Lazy_storage_kind.t) (id : i) (diff : diff i a u) : diffs_item :=
  E k id diff.

Definition make_remove (function_parameter : KId.t) : diffs_item :=
  let 'KId.E k id := function_parameter in
  let 'existT _ __E_'id [k, id] :=
    existT (A := Set) (fun __E_'id => [Lazy_storage_kind.t ** __E_'id]) _
      [k, id] in
  E k id Remove.

Definition item_encoding : Data_encoding.encoding diffs_item :=
  Pervasives.op_atat
    (let arg := Data_encoding.union in
    fun eta => arg None eta)
    (List.map
      (fun function_parameter =>
        let '(tag, Lazy_storage_kind.E k) := function_parameter in
        let ops := get_ops k in
        let OPS := ops in
        let 'existS _ _ OPS := OPS in
        let title := OPS.(OPS.title) in
        Data_encoding.__case_value title None (Data_encoding.Tag tag)
          (Data_encoding.obj3
            (Data_encoding.req None None "kind" (Data_encoding.constant title))
            (Data_encoding.req None None "id"
              OPS.(OPS.Id).(Lazy_storage_kind.ID.encoding))
            (Data_encoding.req None None "diff" (diff_encoding ops)))
          (fun function_parameter =>
            let 'E kind id diff := function_parameter in
            let 'existT _ [__E_'a, __E_'i, __E_'u] [kind, id, diff] :=
              existT (A := [Set ** Set ** Set])
                (fun '[__E_'a, __E_'i, __E_'u] =>
                  [Lazy_storage_kind.t ** __E_'i ** diff __E_'i __E_'a __E_'u])
                [_, _, _] [kind, id, diff] in
            match Lazy_storage_kind.compare k kind with
            | Lazy_storage_kind.Eq => Some (tt, id, diff)
            | (Lazy_storage_kind.Lt | Lazy_storage_kind.Gt) => None
            end)
          (fun function_parameter =>
            let '(_, id, diff) := function_parameter in
            E k id diff)) Lazy_storage_kind.all).

Definition diffs : Set := list diffs_item.

Definition encoding : Data_encoding.encoding (list diffs_item) :=
  Pervasives.op_atat
    (let arg := Data_encoding.def "lazy_storage_diff" in
    fun eta => arg None None eta)
    (Data_encoding.__list_value None item_encoding).

Definition apply (ctxt : Raw_context.t) (diffs : list diffs_item)
  : Lwt.t (Error_monad.tzresult (Raw_context.t * Z.t)) :=
  Error_monad.fold_left_s
    (fun function_parameter =>
      let '(ctxt, total_size) := function_parameter in
      fun function_parameter =>
        let 'E k id diff := function_parameter in
        let 'existT _ [__E_'a, __E_'i, __E_'u] [k, id, diff] :=
          existT (A := [Set ** Set ** Set])
            (fun '[__E_'a, __E_'i, __E_'u] =>
              [Lazy_storage_kind.t ** __E_'i ** diff __E_'i __E_'a __E_'u]) [_,
            _, _] [k, id, diff] in
        let ops := get_ops k in
        Error_monad.op_gtpipeeqquestion (apply_diff ctxt ops id diff)
          (fun function_parameter =>
            let '(ctxt, added_size) := function_parameter in
            let OPS := ops in
            let 'existS _ _ OPS := OPS in
            (ctxt,
              (if OPS.(OPS.Id).(Lazy_storage_kind.ID.is_temp) id then
                total_size
              else
                Z.add total_size added_size)))) (ctxt, Z.zero) diffs.

Definition fresh {i : Set}
  (kind : Lazy_storage_kind.t) (temporary : bool) (ctxt : Raw_context.t)
  : Lwt.t (Error_monad.tzresult (Raw_context.t * i)) :=
  if temporary then
    Error_monad.__return
      (Raw_context.fold_map_temporary_lazy_storage_ids ctxt
        (fun temp_ids => Lazy_storage_kind.Temp_ids.fresh kind temp_ids))
  else
    let OPS := get_ops kind in
    let 'existS _ _ OPS := OPS in
    OPS.(OPS.Next).(Next.incr) ctxt.

Definition init (ctxt : Raw_context.t)
  : Lwt.t (Error_monad.tzresult Raw_context.t) :=
  Error_monad.fold_left_s
    (fun ctxt =>
      fun function_parameter =>
        let '(_tag, Lazy_storage_kind.E k) := function_parameter in
        let OPS := get_ops k in
        let 'existS _ _ OPS := OPS in
        OPS.(OPS.Next).(Next.init) ctxt) ctxt Lazy_storage_kind.all.

Definition cleanup_temporaries (ctxt : Raw_context.context)
  : Lwt.t Raw_context.context :=
  Raw_context.map_temporary_lazy_storage_ids_s ctxt
    (fun temp_ids =>
      Error_monad.op_gtpipeeq
        (Lwt_list.fold_left_s
          (fun ctxt =>
            fun function_parameter =>
              let '(_tag, Lazy_storage_kind.E k) := function_parameter in
              let OPS := get_ops k in
              let 'existS _ _ OPS := OPS in
              Lazy_storage_kind.Temp_ids.fold_s k OPS.(OPS.remove_rec) temp_ids
                ctxt) ctxt Lazy_storage_kind.all)
        (fun ctxt => (ctxt, Lazy_storage_kind.Temp_ids.init))).
