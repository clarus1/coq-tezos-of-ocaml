(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.
Unset Guard Checking.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.

Import Environment.Notations.

Definition lazyt (a : Set) : Set := unit -> a.

Inductive lazy_list_t (a : Set) : Set :=
| LCons :
  a -> lazyt (Lwt.t (Error_monad.tzresult (lazy_list_t a))) -> lazy_list_t a.

Arguments LCons {_}.

Definition lazy_list (a : Set) : Set :=
  Lwt.t (Error_monad.tzresult (lazy_list_t a)).

Fixpoint op_minusminusgt
  (i : (|Compare.Int|).(Compare.S.t)) (j : (|Compare.Int|).(Compare.S.t))
  {struct i} : list (|Compare.Int|).(Compare.S.t) :=
  if (|Compare.Int|).(Compare.S.op_gt) i j then
    nil
  else
    cons i (op_minusminusgt (Pervasives.succ i) j).

Fixpoint op_minusminusminusgt
  (i : (|Compare.Int32|).(Compare.S.t)) (j : (|Compare.Int32|).(Compare.S.t))
  {struct i} : list (|Compare.Int32|).(Compare.S.t) :=
  if (|Compare.Int32|).(Compare.S.op_gt) i j then
    nil
  else
    cons i (op_minusminusminusgt (Int32.succ i) j).

Definition split
  : (|Compare.Char|).(Compare.S.t) -> option (|Compare.Int|).(Compare.S.t) ->
  string -> list string := axiom.

Definition pp_print_paragraph (ppf : Format.formatter) (description : string)
  : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Formatting_gen
        (CamlinternalFormatBasics.Open_box
          (CamlinternalFormatBasics.Format
            CamlinternalFormatBasics.End_of_format ""))
        (CamlinternalFormatBasics.Alpha
          (CamlinternalFormatBasics.Formatting_lit
            CamlinternalFormatBasics.Close_box
            CamlinternalFormatBasics.End_of_format))) "@[%a@]")
    (Format.pp_print_list (Some Format.pp_print_space) Format.pp_print_string)
    (split " " % char None description).

Definition take {A : Set} (n : (|Compare.Int|).(Compare.S.t)) (l : list A)
  : option (list A * list A) :=
  let fix loop {B : Set}
    (acc : list B) (n : (|Compare.Int|).(Compare.S.t)) (xs : list B)
    {struct acc} : option (list B * list B) :=
    if (|Compare.Int|).(Compare.S.op_lteq) n 0 then
      Some ((List.rev acc), xs)
    else
      match xs with
      | [] => None
      | cons x xs => loop (cons x acc) (Pervasives.op_minus n 1) xs
      end in
  loop nil n l.

Definition remove_prefix
  (prefix : (|Compare.String|).(Compare.S.t)) (s : string) : option string :=
  let x := String.length prefix in
  let n := String.length s in
  if
    Pervasives.op_andand ((|Compare.Int|).(Compare.S.op_gteq) n x)
      ((|Compare.String|).(Compare.S.op_eq) (String.sub s 0 x) prefix) then
    Some (String.sub s x (Pervasives.op_minus n x))
  else
    None.

Fixpoint remove_elem_from_list {A : Set}
  (nb : (|Compare.Int|).(Compare.S.t)) (function_parameter : list A) {struct nb}
  : list A :=
  match
    (function_parameter,
      match function_parameter with
      | (cons _ _) as l => (|Compare.Int|).(Compare.S.op_lteq) nb 0
      | _ => false
      end) with
  | ([], _) => nil
  | ((cons _ _) as l, true) => l
  | (cons _ tl, _) => remove_elem_from_list (Pervasives.op_minus nb 1) tl
  end.
