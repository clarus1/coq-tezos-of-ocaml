(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Constants_repr.

Import Environment.Notations.

Definition proposal : Set := (|Protocol_hash|).(S.HASH.t).

Module ballot.
  Record record : Set := Build {
    yays_per_roll : int;
    nays_per_roll : int;
    passes_per_roll : int }.
  Definition with_yays_per_roll yays_per_roll (r : record) :=
    Build yays_per_roll r.(nays_per_roll) r.(passes_per_roll).
  Definition with_nays_per_roll nays_per_roll (r : record) :=
    Build r.(yays_per_roll) nays_per_roll r.(passes_per_roll).
  Definition with_passes_per_roll passes_per_roll (r : record) :=
    Build r.(yays_per_roll) r.(nays_per_roll) passes_per_roll.
End ballot.
Definition ballot := ballot.record.

Definition ballot_encoding : Data_encoding.encoding ballot :=
  Pervasives.op_atat
    (let arg :=
      Data_encoding.conv
        (fun function_parameter =>
          let '{|
            ballot.yays_per_roll := yays_per_roll;
              ballot.nays_per_roll := nays_per_roll;
              ballot.passes_per_roll := passes_per_roll
              |} := function_parameter in
          (yays_per_roll, nays_per_roll, passes_per_roll))
        (fun function_parameter =>
          let '(yays_per_roll, nays_per_roll, passes_per_roll) :=
            function_parameter in
          if
            Pervasives.op_andand
              ((|Compare.Int|).(Compare.S.op_gteq) yays_per_roll 0)
              (Pervasives.op_andand
                ((|Compare.Int|).(Compare.S.op_gteq) nays_per_roll 0)
                (Pervasives.op_andand
                  ((|Compare.Int|).(Compare.S.op_gteq) passes_per_roll 0)
                  ((|Compare.Int|).(Compare.S.op_eq)
                    (Pervasives.op_plus
                      (Pervasives.op_plus yays_per_roll nays_per_roll)
                      passes_per_roll)
                    Constants_repr.__fixed_value.(Constants_repr.fixed.votes_per_roll))))
            then
            {| ballot.yays_per_roll := yays_per_roll;
              ballot.nays_per_roll := nays_per_roll;
              ballot.passes_per_roll := passes_per_roll |}
          else
            Pervasives.invalid_arg "ballot_encoding") in
    fun eta => arg None eta)
    (Data_encoding.obj3
      (Data_encoding.req None None "yays_per_roll" Data_encoding.uint16)
      (Data_encoding.req None None "nays_per_roll" Data_encoding.uint16)
      (Data_encoding.req None None "passes_per_roll" Data_encoding.uint16)).
