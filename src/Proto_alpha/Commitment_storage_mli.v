(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Commitment_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Import Environment.Notations.

Parameter init :
  Raw_context.t -> list Commitment_repr.t ->
  Lwt.t (Error_monad.tzresult Raw_context.t).

Parameter get_opt :
  Raw_context.t -> Blinded_public_key_hash.t ->
  Lwt.t (Error_monad.tzresult (option Tez_repr.t)).

Parameter delete :
  Raw_context.t -> Blinded_public_key_hash.t ->
  Lwt.t (Error_monad.tzresult Raw_context.t).
