(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require TezosOfOCaml.Proto_alpha.Environment.
Import Environment.
Require TezosOfOCaml.Proto_alpha.Storage_description.

Import Environment.Notations.

Parameter t : Set.

Definition raw_level : Set := t.

Parameter encoding : Data_encoding.t raw_level.

Parameter rpc_arg : RPC_arg.arg raw_level.

Parameter pp : Format.formatter -> raw_level -> unit.

Parameter Included_S : {_ : unit & Compare.S.signature (t := raw_level)}.

Definition op_eq : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_eq).

Definition op_ltgt : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_ltgt).

Definition op_lt : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_lt).

Definition op_lteq : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_lteq).

Definition op_gteq : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_gteq).

Definition op_gt : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.op_gt).

Definition compare : raw_level -> raw_level -> int :=
  (|Included_S|).(Compare.S.compare).

Definition equal : raw_level -> raw_level -> bool :=
  (|Included_S|).(Compare.S.equal).

Definition max : raw_level -> raw_level -> raw_level :=
  (|Included_S|).(Compare.S.max).

Definition min : raw_level -> raw_level -> raw_level :=
  (|Included_S|).(Compare.S.min).

Parameter to_int32 : raw_level -> int32.

Parameter of_int32_exn : int32 -> raw_level.

Parameter of_int32 : int32 -> Error_monad.tzresult raw_level.

Parameter diff : raw_level -> raw_level -> int32.

Parameter root : raw_level.

Parameter succ : raw_level -> raw_level.

Parameter pred : raw_level -> option raw_level.

Parameter Index :
  {_ : unit & Storage_description.INDEX.signature (t := raw_level)}.

Module LSet.
  Parameter Included_SET :
    {t : Set & S.SET.signature (elt := raw_level) (t := t)}.
  
  Definition elt := (|Included_SET|).(S.SET.elt).
  
  Definition t := (|Included_SET|).(S.SET.t).
  
  Definition empty : t := (|Included_SET|).(S.SET.empty).
  
  Definition is_empty : t -> bool := (|Included_SET|).(S.SET.is_empty).
  
  Definition mem : elt -> t -> bool := (|Included_SET|).(S.SET.mem).
  
  Definition add : elt -> t -> t := (|Included_SET|).(S.SET.add).
  
  Definition singleton : elt -> t := (|Included_SET|).(S.SET.singleton).
  
  Definition remove : elt -> t -> t := (|Included_SET|).(S.SET.remove).
  
  Definition union : t -> t -> t := (|Included_SET|).(S.SET.union).
  
  Definition inter : t -> t -> t := (|Included_SET|).(S.SET.inter).
  
  Definition diff : t -> t -> t := (|Included_SET|).(S.SET.diff).
  
  Definition compare : t -> t -> int := (|Included_SET|).(S.SET.compare).
  
  Definition equal : t -> t -> bool := (|Included_SET|).(S.SET.equal).
  
  Definition subset : t -> t -> bool := (|Included_SET|).(S.SET.subset).
  
  Definition iter : (elt -> unit) -> t -> unit := (|Included_SET|).(S.SET.iter).
  
  Definition map : (elt -> elt) -> t -> t := (|Included_SET|).(S.SET.map).
  
  Definition fold {a : Set} : (elt -> a -> a) -> t -> a -> a :=
    (|Included_SET|).(S.SET.fold).
  
  Definition for_all : (elt -> bool) -> t -> bool :=
    (|Included_SET|).(S.SET.for_all).
  
  Definition __exists : (elt -> bool) -> t -> bool :=
    (|Included_SET|).(S.SET.__exists).
  
  Definition filter : (elt -> bool) -> t -> t :=
    (|Included_SET|).(S.SET.filter).
  
  Definition partition : (elt -> bool) -> t -> t * t :=
    (|Included_SET|).(S.SET.partition).
  
  Definition cardinal : t -> int := (|Included_SET|).(S.SET.cardinal).
  
  Definition elements : t -> list elt := (|Included_SET|).(S.SET.elements).
  
  Definition min_elt_opt : t -> option elt :=
    (|Included_SET|).(S.SET.min_elt_opt).
  
  Definition max_elt_opt : t -> option elt :=
    (|Included_SET|).(S.SET.max_elt_opt).
  
  Definition choose_opt : t -> option elt :=
    (|Included_SET|).(S.SET.choose_opt).
  
  Definition split : elt -> t -> t * bool * t := (|Included_SET|).(S.SET.split).
  
  Definition find_opt : elt -> t -> option elt :=
    (|Included_SET|).(S.SET.find_opt).
  
  Definition find_first_opt : (elt -> bool) -> t -> option elt :=
    (|Included_SET|).(S.SET.find_first_opt).
  
  Definition find_last_opt : (elt -> bool) -> t -> option elt :=
    (|Included_SET|).(S.SET.find_last_opt).
  
  Definition of_list : list elt -> t := (|Included_SET|).(S.SET.of_list).
  
  Parameter encoding : Data_encoding.t t.
End LSet.
