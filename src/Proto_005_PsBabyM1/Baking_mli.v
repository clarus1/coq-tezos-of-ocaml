(** Generated by coq-of-ocaml *)
Require Import OCaml.OCaml.

Local Set Primitive Projections.
Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Require Import TezosOfOCaml.Proto_005_PsBabyM1.Environment.
Import Environment.Notations.
Require TezosOfOCaml.Proto_005_PsBabyM1.Alpha_context.
Require TezosOfOCaml.Proto_005_PsBabyM1.Misc.

Import Alpha_context.

Import Misc.

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

(* extensible_type_definition `error` *)

Parameter minimal_time :
  Alpha_context.context -> int -> Time.t -> Lwt.t (Error_monad.tzresult Time.t).

Parameter check_baking_rights :
  Alpha_context.context -> Alpha_context.Block_header.contents -> Time.t ->
  Lwt.t
    (Error_monad.tzresult (Alpha_context.public_key * Alpha_context.Period.t)).

Parameter endorsement_rights :
  Alpha_context.context -> Alpha_context.Level.t ->
  Lwt.t
    (Error_monad.tzresult
      ((|Signature.Public_key_hash|).(S.SPublic_key_hash.Map).(S.INDEXES_Map.t)
        (Alpha_context.public_key * list int * bool))).

Parameter check_endorsement_rights :
  Alpha_context.context -> (|Chain_id|).(S.HASH.t) ->
  Alpha_context.Operation.t ->
  Lwt.t (Error_monad.tzresult (Alpha_context.public_key_hash * list int * bool)).

Parameter baking_reward :
  Alpha_context.context -> int -> int ->
  Lwt.t (Error_monad.tzresult Alpha_context.Tez.t).

Parameter endorsing_reward :
  Alpha_context.context -> int -> int ->
  Lwt.t (Error_monad.tzresult Alpha_context.Tez.t).

Parameter baking_priorities :
  Alpha_context.context -> Alpha_context.Level.t ->
  Misc.lazy_list Alpha_context.public_key.

Parameter first_baking_priorities :
  Alpha_context.context -> option int -> Alpha_context.public_key_hash ->
  Alpha_context.Level.t -> Lwt.t (Error_monad.tzresult (list int)).

Parameter check_signature :
  Alpha_context.Block_header.block_header -> (|Chain_id|).(S.HASH.t) ->
  Alpha_context.public_key -> Lwt.t (Error_monad.tzresult unit).

Parameter check_header_proof_of_work_stamp :
  Alpha_context.Block_header.shell_header ->
  Alpha_context.Block_header.contents -> int64 -> bool.

Parameter check_proof_of_work_stamp :
  Alpha_context.context -> Alpha_context.Block_header.block_header ->
  Lwt.t (Error_monad.tzresult unit).

Parameter check_fitness_gap :
  Alpha_context.context -> Alpha_context.Block_header.block_header ->
  Lwt.t (Error_monad.tzresult unit).

Parameter dawn_of_a_new_cycle :
  Alpha_context.context ->
  Lwt.t (Error_monad.tzresult (option Alpha_context.Cycle.t)).

Parameter earlier_predecessor_timestamp :
  Alpha_context.context -> Alpha_context.Level.t ->
  Lwt.t (Error_monad.tzresult Alpha_context.Timestamp.t).

Parameter minimum_allowed_endorsements :
  Alpha_context.context -> Alpha_context.Period.t -> int.

Parameter minimal_valid_time :
  Alpha_context.context -> int -> int -> Lwt.t (Error_monad.tzresult Time.t).
