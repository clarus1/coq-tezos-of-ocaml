notations = <<END
Module Notations.
  Notation "'let=' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgteq X (fun x => Y))
    (at level 200, x ident, X at level 100, Y at level 200).

  Notation "'let=' ' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgteq X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).

  Notation "'let=?' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgteqquestion X (fun x => Y))
    (at level 200, x ident, X at level 100, Y at level 200).

  Notation "'let=?' ' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgteqquestion X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).

  Notation "'let?' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgtquestion X (fun x => Y))
    (at level 200, x ident, X at level 100, Y at level 200).

  Notation "'let?' ' x ':=' X 'in' Y" :=
    (Error_monad.op_gtgtquestion X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).
End Notations.
END

module_List = <<END
Module List.
  Definition t (A : Set) : Set := list A.
  Parameter length : forall {a : Set}, list a -> int.
  Parameter compare_lengths : forall {a b : Set}, list a -> list b -> int.
  Parameter compare_length_with : forall {a : Set}, list a -> int -> int.
  Parameter __cons_value : forall {a : Set}, a -> list a -> list a.
  Parameter hd : forall {a : Set}, list a -> a.
  Parameter tl : forall {a : Set}, list a -> list a.
  Parameter nth_opt : forall {a : Set}, list a -> int -> option a.
  Definition rev : forall {a : Set}, list a -> list a :=
    fun {_} => List.rev.
  Parameter init : forall {a : Set}, int -> (int -> a) -> list a.
  Parameter append : forall {a : Set}, list a -> list a -> list a.
  Parameter rev_append : forall {a : Set}, list a -> list a -> list a.
  Parameter concat : forall {a : Set}, list (list a) -> list a.
  Parameter flatten : forall {a : Set}, list (list a) -> list a.
  Parameter iter : forall {a : Set}, (a -> unit) -> list a -> unit.
  Parameter iteri : forall {a : Set}, (int -> a -> unit) -> list a -> unit.
  Definition map : forall {a b : Set}, (a -> b) -> list a -> list b :=
    fun {_ _} => List.map.
  Parameter mapi : forall {a b : Set}, (int -> a -> b) -> list a -> list b.
  Parameter rev_map : forall {a b : Set}, (a -> b) -> list a -> list b.
  Parameter filter_map : forall {a b : Set}, (a -> option b) -> list a -> list b.
  Parameter fold_left : forall {a b : Set}, (a -> b -> a) -> a -> list b -> a.
  Parameter fold_right : forall {a b : Set}, (a -> b -> b) -> list a -> b -> b.
  Parameter iter2 : forall {a b : Set}, (a -> b -> unit) -> list a -> list b -> unit.
  Parameter map2 : forall {a b c : Set}, (a -> b -> c) -> list a -> list b -> list c.
  Parameter rev_map2 : forall {a b c : Set}, (a -> b -> c) -> list a -> list b -> list c.
  Parameter fold_left2 : forall {a b c : Set}, (a -> b -> c -> a) -> a -> list b -> list c -> a.
  Parameter fold_right2 : forall {a b c : Set}, (a -> b -> c -> c) -> list a -> list b -> c -> c.
  Parameter for_all : forall {a : Set}, (a -> bool) -> list a -> bool.
  Parameter __exists : forall {a : Set}, (a -> bool) -> list a -> bool.
  Parameter for_all2 : forall {a b : Set}, (a -> b -> bool) -> list a -> list b -> bool.
  Parameter __exists2 : forall {a b : Set}, (a -> b -> bool) -> list a -> list b -> bool.
  Parameter find_opt : forall {a : Set}, (a -> bool) -> list a -> option a.
  Parameter filter : forall {a : Set}, (a -> bool) -> list a -> list a.
  Parameter find_all : forall {a : Set}, (a -> bool) -> list a -> list a.
  Parameter partition : forall {a : Set}, (a -> bool) -> list a -> list a * list a.
  Parameter split : forall {a b : Set}, list (a * b) -> list a * list b.
  Parameter combine : forall {a b : Set}, list a -> list b -> list (a * b).
  Parameter sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  Parameter stable_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  Parameter fast_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  Parameter sort_uniq : forall {a : Set}, (a -> a -> int) -> list a -> list a.
  Parameter merge : forall {a : Set}, (a -> a -> int) -> list a -> list a -> list a.
End List.
END

module_Z = <<END
Module Z.
  Definition t := Z.
  Definition zero := 0.
  Definition one := 1.
  Definition succ z := Z.add z 1.
  Definition abs := Z.abs.
  Definition neg z := -z.
  Definition add := Z.add.
  Definition sub := Z.sub.
  Definition mul := Z.mul.
  Parameter ediv_rem : t -> t -> t * t.
  Parameter logand : t -> t -> t.
  Parameter logor : t -> t -> t.
  Parameter logxor : t -> t -> t.
  Parameter lognot : t -> t.
  Parameter shift_left : t -> int -> t.
  Parameter shift_right : t -> int -> t.
  Parameter to_string : t -> string.
  Parameter pp : Format.formatter -> t -> unit.
  Parameter of_string : string -> t.
  Parameter to_int64 : t -> int64.
  Parameter of_int64 : int64 -> t.
  Parameter to_int : t -> int.
  Parameter of_int : int -> t.
  Parameter to_bits : option int -> t -> Bytes.t.
  Parameter of_bits : Bytes.t -> t.
  Definition equal := Z.eqb.
  Parameter compare : t -> t -> int.
  Parameter numbits : t -> int.
End Z.
END

module_Lwt = <<END
Module Lwt.
  Definition t (a : Set) : Set := a.
  Definition __return {a : Set} (x : a) : t a := x.
  Definition bind {a b : Set} (x : t a) (f : a -> t b) : t b := f x.
  Definition op_gtgteq {a b : Set} : t a -> (a -> t b) -> t b := bind.
  Definition op_eqltlt {a b : Set} (f : a -> t b) (x : t a) : t b := f x.
  Definition map {a b : Set} (f : a -> b) (x : t a) : t b := f x.
  Definition op_gtpipeeq {a b : Set} (x : t a) (f : a -> b) : t b := f x.
  Definition op_eqpipelt {a b : Set} : (a -> b) -> t a -> t b := map.
  Definition return_unit : t unit := tt.
  Definition return_none {a : Set} : t (option a) := None.
  Definition return_nil {a : Set} : t (list a) := [].
  Definition return_true : t bool := true.
  Definition return_false : t bool := false.
  Parameter join : list (t unit) -> t unit.
  Parameter op_ltandgt : t unit -> t unit -> t unit.
End Lwt.
END

$environment_content = File.read("Environment_mli.v")

def use_module!(module_name, module_content)
  $environment_content.gsub!(
    "Module #{module_name}.",
    "Module Type #{module_name}_type."
  )
  $environment_content.gsub!(
    "End #{module_name}.\n",
    "End #{module_name}_type.\n\n#{module_content}\nModule #{module_name}_check : #{module_name}_type := #{module_name}.\n"
  )
end

use_module!("List", module_List)
use_module!("Z", module_Z)
use_module!("Lwt", module_Lwt)

File.open("Environment_mli.v", "w") do |file|
  file << $environment_content
  file << "\n"
  file << notations
end
