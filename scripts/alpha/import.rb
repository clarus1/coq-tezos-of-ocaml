# Import the protocol using `coq-of-ocaml`

if ARGV.size < 3 then
  puts "Usage:"
  puts "  ruby import.rb tezos_path protocol_path target_path"
  exit(1)
end

tezos_path, protocol_path, target_path = ARGV
full_protocol_path = File.join(tezos_path, protocol_path)

ml_path = File.join(tezos_path, "_build", "default", "src", "lib_protocol_environment", "sigs", "v1.ml")
ml_content = File.read(ml_path, encoding: "UTF-8")
mli_content = ml_content.split("\n")[1..-2].select {|line| line[0] != "#"}.join("\n")
File.open("environment.mli", "w") do |file|
  file << mli_content
end
system("coq-of-ocaml -config config-environment.json environment.mli")
system("ruby add_notations.rb")
system("mv Environment_mli.v #{File.join(target_path, "Environment.v")}")

for ocaml_file_name in Dir.glob(File.join(full_protocol_path, "*.{ml,mli}")).sort
  system("cd #{full_protocol_path} && coq-of-ocaml -config #{File.expand_path("config.json")} #{File.basename(ocaml_file_name)}")
end

system("cp #{full_protocol_path}/*.v #{target_path}")
